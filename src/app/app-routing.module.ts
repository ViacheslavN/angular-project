import { OrdersComponent } from './orders/orders.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ProductGuard } from './catalog/category-viewer/product-viewer/product-guard.service';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailStartComponent } from './catalog/category-viewer/product-viewer/product-detail-start/product-detail-start.component';
import { ProductViewerStartComponent } from './catalog/category-viewer/product-viewer-start/product-viewer-start.component';
import { ProductDetailComponent } from './catalog/category-viewer/product-viewer/product-detail/product-detail.component';
import { ProductViewerComponent } from './catalog/category-viewer/product-viewer/product-viewer.component';
import { CatalogComponent } from './catalog/catalog.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoryGuard } from './catalog/category-viewer/product-viewer/category-guard.service';

const appRoutes: Routes = [
    { path: '', redirectTo: '/catalog', pathMatch: 'full' },
    { path: 'catalog', component: CatalogComponent, children: [
        { path: '', component: ProductViewerStartComponent },
        { path: ':categoryId', canActivate: [CategoryGuard], component: ProductViewerComponent, children: [
            { path: '', component: ProductDetailStartComponent },
            { path: ':productId', canActivate: [ProductGuard], component: ProductDetailComponent }
        ] }
    ] },
    { path: 'cart', component: ShoppingCartComponent },
    { path: 'orders', component: OrdersComponent },
    { path: 'not-found', component: PageNotFoundComponent },
    { path: '**', redirectTo: '/not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}