import { ProductService } from './product.service';
import { NotifierService } from './notifier.service';
import { CartItem } from './../shopping-cart/cart-item/cart-item.model';
import { StockService } from './stock.service';
import { PriceService } from './price.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Product } from './product.model';
import { Subject } from "rxjs/Subject";

@Injectable()
export class ShoppingCartService {
    private _cartItems: CartItem[] = [];
    public cartItemsChanged: Subject<CartItem[]> = new Subject<CartItem[]>();
    public qtyInputChanged: EventEmitter<void> = new EventEmitter<void>();
    readonly LOCAL_KEY: string = 'shoppingCart';

    constructor(private _priceService: PriceService,
        private _stockService: StockService,
        private _notifierService: NotifierService,
        private _productService: ProductService) {
    }

    public getCartItems() {
        return this._cartItems.slice();
    }

    public isProductinCart(product: Product): boolean {
        const result: CartItem | undefined = this._cartItems.find((item: CartItem, index: number, array: CartItem[]) => {
            return item.product === product;
        });
        return !!result;
    }

    public addProductToCart(product: Product, quantity: number): void {
        if (this.isProductinCart(product)) {
            const cartItem: CartItem = this._cartItems.find((item: CartItem, index: number, array: CartItem[]) => {
                return item.product === product;
            });
            const qtyReserved: number = this._stockService.getProductQtyReserved(product.id);
            const newQtyReserved: number = qtyReserved + quantity;
            this._stockService.setProductQtyReserved(product.id, newQtyReserved);
            cartItem.updateQty(newQtyReserved);
            this._notifierService.setMessage('The shopping cart was updated!').showNotification();
        } else {
            const price: number = this._priceService.getProductPrice(product.id);
            const cartItem = new CartItem(product, quantity, price);
            this._stockService.setProductQtyReserved(product.id, quantity);
            this._cartItems.push(cartItem);
            this._notifierService.setMessage('The product was added to shopping cart!').showNotification();
        }
    }

    public deleteItemFromCart(index: number): void {
        if (typeof this._cartItems[index] === 'undefined') {
            return;
        }

        const product: Product = this._cartItems[index].product;
        this._stockService.setProductQtyReserved(product.id, 0);
        this._cartItems.splice(index, 1);
        this.cartItemsChanged.next(this._cartItems.slice());
        this._notifierService.setMessage('The item was deleted!').showNotification();
    }

    public saveCartItems(): void {
        console.log('saving cart items');
        let cartItemsArr: { productId: string, quantity: number }[] = [];
        this._cartItems.forEach((cartItem: CartItem, index: number, array: CartItem[]) => {
            const item: { productId: string, quantity: number } = { 'productId': cartItem.product.id, 'quantity': cartItem.quantity };
            cartItemsArr.push(item);
        });

        const cartItemsJsonString: string = JSON.stringify(cartItemsArr);
        localStorage.setItem(this.LOCAL_KEY, cartItemsJsonString);
    }

    public restoreCartItems(): void {
        const cartItemsJsonString: string = localStorage.getItem(this.LOCAL_KEY);
        if (cartItemsJsonString === null) {
            return;
        }

        let cartItemsArr = JSON.parse(cartItemsJsonString);
        cartItemsArr.forEach((cartItem: { productId: string, quantity: number },
            index: number,
            array: { productId: string, quantity: number }[]) => {
            const product: Product = this._productService.getProductById(cartItem.productId);
            const quantity: number = cartItem.quantity;
            this._stockService.setProductQtyReserved(product.id, quantity);
            const price = this._priceService.getProductPrice(product.id);

            const cartItemObj: CartItem = new CartItem(product, quantity, price);
            this._cartItems.push(cartItemObj);
        });

    }

    public cleanShoppingCart(): void {
        this._cartItems.forEach((cartItem: CartItem, index: number, array: CartItem[]) => {
            const product = cartItem.product;
            this._stockService.setProductQtyReserved(product.id, 0);
        });
        this._cartItems = [];
        this.cartItemsChanged.next(this._cartItems.slice());
    }
}