import { EventEmitter } from '@angular/core';

export class StockService {

    private _quantityByProductId: Map<string, number> = new Map<string, number>();
    private _qtyReservedByProductId: Map<string, number> = new Map<string, number>();

    public stockChanged: EventEmitter<void> = new EventEmitter<void>();

    constructor() {
        this._quantityByProductId.set('1', 10);
        this._quantityByProductId.set('2', 11);
        this._quantityByProductId.set('3', 12);
        this._quantityByProductId.set('4', 13);
        this._quantityByProductId.set('5', 14);
        this._quantityByProductId.set('6', 15);
        this._quantityByProductId.set('7', 16);
        this._quantityByProductId.set('8', 17);
        this._quantityByProductId.set('9', 18);
        this._quantityByProductId.set('10', 19);
        this._quantityByProductId.set('11', 21);
        this._quantityByProductId.set('12', 2);
        this._quantityByProductId.set('13', 2);
        this._quantityByProductId.set('14', 4);
        this._quantityByProductId.set('15', 3);
        this._quantityByProductId.set('16', 6);
        this._quantityByProductId.set('17', 7);
        this._quantityByProductId.set('18', 8);
    }

    public getProductQty(productId: string): number | undefined {
        const productQty: number = this._quantityByProductId.get(productId);
        return productQty;
    }

    public setProductQty(productId: string, newQty: number): void {
        this._quantityByProductId.set(productId, newQty);
        this.stockChanged.emit();
    }

    public getProductQtyReserved(productId: string): number {
        const result: number | undefined = this._qtyReservedByProductId.get(productId);
        if (typeof result === 'undefined') {
            return 0;
        } else {
            return result;
        }
    }

    public setProductQtyReserved(productId: string, newQtyReserved: number): void {
        this._qtyReservedByProductId.set(productId, newQtyReserved);
        this.stockChanged.emit();
    }
    
}