export class PriceService {
    private _priceByProductId: Map<string, number> = new Map<string, number>();

    constructor() {
        this._priceByProductId.set('2', 12);
        this._priceByProductId.set('1', 13);
        this._priceByProductId.set('3', 16);
        this._priceByProductId.set('4', 17);
        this._priceByProductId.set('5', 19);
        this._priceByProductId.set('6', 15);
        this._priceByProductId.set('7', 13);
        this._priceByProductId.set('8', 10);
        this._priceByProductId.set('9', 20);
        this._priceByProductId.set('10', 20);
        this._priceByProductId.set('11', 30);
        this._priceByProductId.set('12', 50);
        this._priceByProductId.set('13', 10);
        this._priceByProductId.set('14', 10);
        this._priceByProductId.set('15', 20);
        this._priceByProductId.set('16', 60);
        this._priceByProductId.set('17', 70);
        this._priceByProductId.set('18', 80);
    }

    public getProductPrice(productId: string): number | undefined {
        const productPrice: number = this._priceByProductId.get(productId);
        return productPrice;
    }
}