export class Product {
    private _id: string;
    private _name: string;
    private _shortDescription: string;
    private _description: string;
    private _previewImagePath: string;
    private _imagePath: string;

    constructor(
        id: string,
        name: string,
        shortDescription: string,
        description: string,
        previewImagePath: string,
        imagePath: string
    ) {
        this._id = id;
        this._name = name;
        this._shortDescription = shortDescription;
        this._description = description;
        this._previewImagePath = previewImagePath;
        this._imagePath = imagePath;
    }

    get id(): string {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get shortDescription(): string {
        return this._shortDescription;
    }

    get description(): string {
        return this._description;
    }

    get previewImagePath(): string {
        return this._previewImagePath;
    }

    get imagePath(): string {
        return this._imagePath;
    }
}