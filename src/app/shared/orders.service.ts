import { NotifierService } from './notifier.service';
import { ShoppingCartService } from './shopping-cart.service';
import { StockService } from './stock.service';
import { Injectable } from '@angular/core';
import { CartItem } from './../shopping-cart/cart-item/cart-item.model';
import { Order } from './../orders/order-item/order.model';

@Injectable()
export class OrdersService {
    private _orders: Order[] = [];

    constructor(private _stockService: StockService,
    private _shoppingCartService: ShoppingCartService,
    private _notifierService: NotifierService) { }

    public getOrders(): Order[] {
        return this._orders;
    }

    public createOrder(cartItems: CartItem[],
        userInfo: { userFirstName: string, userLastName: string, email: string, phone: string, address: string, comment: string }): void {
        cartItems.forEach((cartItem: CartItem, index: number, array: CartItem[]) => {
            const product = cartItem.product;
            const quantity = this._stockService.getProductQty(product.id);
            const qtyReserved = this._stockService.getProductQtyReserved(product.id);
            const newQtyAvailable = quantity - qtyReserved;
            this._stockService.setProductQtyReserved(product.id, 0);
            this._stockService.setProductQty(product.id, newQtyAvailable);
        });
        const order = new Order(cartItems, userInfo);
        this._orders.push(order);
        this._shoppingCartService.cleanShoppingCart();
        this._notifierService.setMessage('The order was placed!').showNotification();
    }
}