import { EventEmitter } from '@angular/core';

export class NotifierService {
    private _message: string;

    public fireNotification: EventEmitter<string> = new EventEmitter<string>();

    constructor() { }

    public setMessage(message: string): this {
        this._message = message;
        return this;
    }

    public showNotification() {
        this.fireNotification.emit(this._message);
    }

}