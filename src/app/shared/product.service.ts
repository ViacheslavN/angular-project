import { Product } from './product.model';

export class ProductService {

    private _productsIdsByCategoryId: Map<string, string[]> = new Map<string, string[]>();
    private _products: Product[] = [
        new Product('1', 'Product 1', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('3', 'Product 3', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('4', 'Product 4', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('5', 'Product 5', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('6', 'Product 6', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('7', 'Product 7', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('2', 'Product 2', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('8', 'Product 8', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('9', 'Product 9', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('10', 'Product 10', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('11', 'Product 11', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('12', 'Product 12', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('13', 'Product 13', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('14', 'Product 14', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('15', 'Product 15', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('16', 'Product 16', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('17', 'Product 17', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg'),
        new Product('18', 'Product 18', 'Short description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias magnam consequuntur, in iusto, a expedita adipisci facere quasi natus possimus! A est, velit odio sequi totam, laudantium ut esse, doloribus tempore quasi pariatur reprehenderit veniam commodi ullam laborum porro aperiam quam eos blanditiis, necessitatibus ratione. Eius, ex pariatur provident placeat.', '/assets/img/food.jpg', '/assets/img/food_full.jpg')
    ];

    constructor() {
        this._productsIdsByCategoryId.set('1', ['1', '2', '3', '4', '5', '6']);
        this._productsIdsByCategoryId.set('2', ['7', '8', '9', '10', '11', '12']);
        this._productsIdsByCategoryId.set('3', ['13', '14', '15', '16', '17', '18']);
    }

    public getProductsByCategoryId(categoryId: string): Product[] {
        let result: Product[] = [];
        const productIds: string[] = this._productsIdsByCategoryId.get(categoryId);
        if (typeof productIds === 'undefined') {
            return result;
        }
        productIds.forEach((id: string, index: number, array: string[]) => {
            let product: Product | undefined = this.getProductById(id);
            if (typeof product !== 'undefined') {
                result.push(product);
            }
        });
        return result;
    }

    public getProductById(productId: string): Product | undefined {
        const findProduct: Product = this._products.find((product: Product, index: number, array: Product[]) => {
            return product.id === productId;
        });
        return findProduct;
    }

    public isProductInCategory(categoryId: string, findId: string): boolean {
        const productIds: string[] | undefined = this._productsIdsByCategoryId.get(categoryId);
        if (typeof productIds === 'undefined') {
            return false;
        }
        const result: string | undefined = productIds.find((id: string, index: number, array: string[]) => {
            return id === findId;
        });
        return !!result;
    }
}