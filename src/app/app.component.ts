import { ShoppingCartService } from './shared/shopping-cart.service';
import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {
  @HostListener('window:beforeunload') saveCart() {
    this._shoppingCartService.saveCartItems();
  }

  constructor(private _shoppingCartService: ShoppingCartService) {
    this._shoppingCartService.restoreCartItems();
  }
}
