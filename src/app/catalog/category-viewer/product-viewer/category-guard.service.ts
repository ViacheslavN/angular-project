import { CategoryService } from './../category.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class CategoryGuard implements CanActivate {
    constructor(private _router: Router, private _categoryService: CategoryService) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const categoryId: string = route.params['categoryId'];
        if (this._categoryService.isCategoryExists(categoryId)) {
            return true;
        } else {
            this._router.navigate(['/not-found']);
            return false;
        }
    }
}