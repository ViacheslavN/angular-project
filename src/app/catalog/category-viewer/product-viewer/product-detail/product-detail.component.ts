import { ShoppingCartService } from './../../../../shared/shopping-cart.service';
import { ProductService } from './../../../../shared/product.service';
import { PriceService } from './../../../../shared/price.service';
import { Product } from './../../../../shared/product.model';
import { StockService } from './../../../../shared/stock.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  private _productId: string;
  public product: Product;
  public quantity: number;
  public quantityReserved: number;
  public price: number;
  public addToCartForm: FormGroup;

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _stockService: StockService,
    private _priceService: PriceService,
    private _productService: ProductService,
    private _shoppingCartService: ShoppingCartService) {

  }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this._productId = params['productId'];
      const product = this._productService.getProductById(this._productId);
      if (typeof product === 'undefined') {
        throw new Error('Invalid product Id!');
      }
      this.product = product;

      const productQty = this._stockService.getProductQty(this.product.id);
      if (typeof productQty === 'undefined') {
        this.quantity = 0;
      } else {
        this.quantity = productQty;
      }

      this.quantityReserved = this._stockService.getProductQtyReserved(this.product.id);

      const productPrice = this._priceService.getProductPrice(this.product.id);
      if (typeof productPrice === 'undefined') {
        this.price = 0;
        this.quantity = 0;
      } else {
        this.price = productPrice;
      }

      this.addToCartForm = new FormGroup({
        'quantity': new FormControl('1', [Validators.required, Validators.pattern(/^[1-9]+[0-9]*$/), this.isQtyAvailable.bind(this)]),
      });
    });

    this._stockService.stockChanged.subscribe(() => {
      this.quantity = this._stockService.getProductQty(this.product.id);
      this.quantityReserved = this._stockService.getProductQtyReserved(this.product.id);
    });
  }

  private isQtyAvailable(control: FormControl): { [s: string]: boolean } | null {
    const value = +control.value;
    if (this.quantity - this.quantityReserved - value < 0) {
      return { 'moreThanAvailable': true };
    }
    return null;
  }

  onSubmit() {
    const value: string = this.addToCartForm.get('quantity').value;
    this._shoppingCartService.addProductToCart(this.product, +value);
    this.addToCartForm.patchValue({ 'quantity': '1' });
  }

}
