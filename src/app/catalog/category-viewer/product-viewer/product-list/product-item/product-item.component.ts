import { PriceService } from './../../../../../shared/price.service';
import { StockService } from './../../../../../shared/stock.service';
import { Product } from './../../../../../shared/product.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit {
  @Input() public product: Product;
  public quantity: number;
  public quantityReserved: number;
  public price: number;

  constructor(private _stockService: StockService, private _priceService: PriceService) { }

  ngOnInit() {
    this._stockService.stockChanged.subscribe(() => {
      const productQty = this._stockService.getProductQty(this.product.id);
      if (typeof productQty === 'undefined') {
        this.quantity = 0;
      } else {
        this.quantity = productQty;
      }
      this.quantityReserved = this._stockService.getProductQtyReserved(this.product.id);
    });

    const productQty = this._stockService.getProductQty(this.product.id);
    if (typeof productQty === 'undefined') {
      this.quantity = 0;
    } else {
      this.quantity = productQty;
    }
    this.quantityReserved = this._stockService.getProductQtyReserved(this.product.id);
    const productPrice = this._priceService.getProductPrice(this.product.id);
    if (typeof productPrice === 'undefined') {
      this.price = 0;
      this.quantity = 0;
    } else {
      this.price = productPrice;
    }
  }

}
