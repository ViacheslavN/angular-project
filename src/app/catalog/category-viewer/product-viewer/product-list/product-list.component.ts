import { ProductService } from './../../../../shared/product.service';
import { Product } from './../../../../shared/product.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  private _categoryId: string;
  public categoryProducts: Product[];

  constructor(private _route: ActivatedRoute,
    private _router: Router,
    private _productService: ProductService) {

  }

  ngOnInit() {
    this._route.params.subscribe((params: Params) => {
      this._categoryId = params['categoryId'];
      this.categoryProducts = this._productService.getProductsByCategoryId(this._categoryId);
    });
  }

}
