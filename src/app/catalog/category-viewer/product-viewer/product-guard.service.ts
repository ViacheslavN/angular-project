import { ProductService } from './../../../shared/product.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ProductGuard implements CanActivate {
    constructor(private _router: Router, private _productService: ProductService) { }

    canActivate(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const productId: string = route.params['productId'];
        const categoryId: string = route.parent.params['categoryId'];
        if (this._productService.isProductInCategory(categoryId, productId)) {
            return true;
        } else {
            this._router.navigate(['/not-found']);
            return false;
        }
    }
}