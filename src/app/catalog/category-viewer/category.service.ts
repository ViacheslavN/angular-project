import { Product } from './../../shared/product.model';
import { Category } from './category.model';

export class CategoryService {
    private _categories: Category[] = [
        new Category('1', 'First Category', 'Category description', 'Full description',
            'imagepath', 'imagepath'),
        new Category('2', 'Second Category', 'Category description', 'Full description',
            'imagepath', 'imagepath'),
        new Category('3', 'Third Category', 'Category description', 'Full description',
            'imagepath', 'imagepath'),
    ];

    public getCategories(): Category[] {
        return this._categories.slice();
    }

    public isCategoryExists(checkId: string): boolean {
        const result: Category | undefined = this._categories.find((category: Category, index: number, array: Category[]) => {
            return category.id === checkId;
        });
        return !!result;
    }
}