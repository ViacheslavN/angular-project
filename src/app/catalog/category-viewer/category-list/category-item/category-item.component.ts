import { Category } from './../../category.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'category-item',
  templateUrl: './category-item.component.html',
  styleUrls: ['./category-item.component.css']
})
export class CategoryItemComponent implements OnInit {
  @Input() public category: Category;

  constructor() { }

  ngOnInit() {
  }

}
