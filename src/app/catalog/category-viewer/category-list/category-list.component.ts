import { Category } from './../category.model';
import { CategoryService } from './../category.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {
  public categories: Category[];

  constructor(private _categoryService: CategoryService) { }

  ngOnInit() {
    this.categories = this._categoryService.getCategories();
  }

}
