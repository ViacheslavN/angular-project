import { OrdersService } from './shared/orders.service';
import { CategoryService } from './catalog/category-viewer/category.service';
import { StockService } from './shared/stock.service';
import { PriceService } from './shared/price.service';
import { ProductService } from './shared/product.service';
import { ShoppingCartService } from './shared/shopping-cart.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { UserComponent } from './header/user/user.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CatalogComponent } from './catalog/catalog.component';
import { CategoryViewerComponent } from './catalog/category-viewer/category-viewer.component';
import { CategoryListComponent } from './catalog/category-viewer/category-list/category-list.component';
import { ProductViewerComponent } from './catalog/category-viewer/product-viewer/product-viewer.component';
import { ProductListComponent } from './catalog/category-viewer/product-viewer/product-list/product-list.component';
import { ProductDetailComponent } from './catalog/category-viewer/product-viewer/product-detail/product-detail.component';
import { CategoryItemComponent } from './catalog/category-viewer/category-list/category-item/category-item.component';
import { ProductItemComponent } from './catalog/category-viewer/product-viewer/product-list/product-item/product-item.component';
import { ProductViewerStartComponent } from './catalog/category-viewer/product-viewer-start/product-viewer-start.component';
import { ProductDetailStartComponent } from './catalog/category-viewer/product-viewer/product-detail-start/product-detail-start.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CategoryGuard } from './catalog/category-viewer/product-viewer/category-guard.service';
import { ProductGuard } from './catalog/category-viewer/product-viewer/product-guard.service';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { CartItemComponent } from './shopping-cart/cart-item/cart-item.component';
import { OrderFormComponent } from './shopping-cart/order-form/order-form.component';
import { NotifierComponent } from './notifier/notifier.component';
import { NotifierService } from './shared/notifier.service';
import { OrdersComponent } from './orders/orders.component';
import { OrderItemComponent } from './orders/order-item/order-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserComponent,
    FooterComponent,
    NavbarComponent,
    CatalogComponent,
    CategoryViewerComponent,
    CategoryListComponent,
    ProductViewerComponent,
    ProductListComponent,
    ProductDetailComponent,
    CategoryItemComponent,
    ProductItemComponent,
    ProductViewerStartComponent,
    ProductDetailStartComponent,
    PageNotFoundComponent,
    ShoppingCartComponent,
    CartItemComponent,
    OrderFormComponent,
    NotifierComponent,
    OrdersComponent,
    OrderItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ProductService,
    PriceService,
    StockService,
    CategoryGuard,
    CategoryService,
    ProductGuard,
    ShoppingCartService,
    NotifierService,
    OrdersService],
  bootstrap: [AppComponent]
})
export class AppModule { }