import { NotifierService } from './../shared/notifier.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'notifier',
  templateUrl: './notifier.component.html',
  styleUrls: ['./notifier.component.css']
})
export class NotifierComponent implements OnInit {
  public message: string = 'test message';
  public show: boolean = false;

  constructor(private _notifierService: NotifierService) { }

  ngOnInit() {
    this._notifierService.fireNotification.subscribe((message: string) => {
      this.message = message;
      this.show = true;
      setTimeout(() => { this.show = false; }, 2000);
    });
  }

}
