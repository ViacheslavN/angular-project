import { Subject } from 'rxjs/Subject';
import { CartItemComponent } from './cart-item/cart-item.component';
import { CartItem } from './cart-item/cart-item.model';
import { ShoppingCartService } from './../shared/shopping-cart.service';
import { Component, OnInit, OnDestroy, ViewChildren, QueryList, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChildren('cartItem') cartItemsComp: QueryList<CartItemComponent>;
  public cartItems: CartItem[] = [];
  private _subscription: Subscription;
  public canPlaceOrder: boolean = false;

  constructor(private _shoppingCartService: ShoppingCartService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.cartItems = this._shoppingCartService.getCartItems();

    this._subscription = this._shoppingCartService.cartItemsChanged.subscribe((cartItems: CartItem[]) => {
      this.cartItems = cartItems;
    });

    this._shoppingCartService.qtyInputChanged.subscribe(() => {
      this.onQtyInputChanged();
    });

  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.onQtyInputChanged();
    this.cartItemsComp.changes.subscribe((cartItemsComp) => {
      this.onQtyInputChanged();
      this.cdr.detectChanges();
    });
  }

  private onQtyInputChanged(): void {
    const hasInvalidInput = this.cartItemsComp.some((cartItemComp: CartItemComponent, index: number, array: CartItemComponent[]) => {
      return cartItemComp.isValid === false;
    });
    this.canPlaceOrder = !hasInvalidInput;
  }

}
