import { OrdersService } from './../../shared/orders.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import { ShoppingCartService } from './../../shared/shopping-cart.service';
import { CartItem } from './../cart-item/cart-item.model';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent implements OnInit, OnDestroy {
  @Input() public canPlaceOrder: boolean;
  private _subscription: Subscription;
  public cartItems: CartItem[];
  public total: number;
  public orderForm: FormGroup;

  constructor(private _shoppingCartService: ShoppingCartService,
  private _ordersService: OrdersService) { }

  private calculateTotal(): void {
    let total = 0;
    this.cartItems.forEach((cartItem: CartItem, index: number, array: CartItem[]) => {
      total += cartItem.total;
    });
    this.total = total;
  }

  ngOnInit() {
    this.cartItems = this._shoppingCartService.getCartItems();
    this.calculateTotal();

    this._subscription = this._shoppingCartService.cartItemsChanged.subscribe((cartItems: CartItem[]) => {
      this.cartItems = cartItems;
      this.calculateTotal();
    });

    this._shoppingCartService.qtyInputChanged.subscribe(() => {
      this.calculateTotal();
    });

    this.orderForm = new FormGroup({
      'userFirstName': new FormControl(null, Validators.required),
      'userLastName': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'phone': new FormControl(null, [Validators.required, Validators.pattern(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)]),
      'address': new FormControl(null, Validators.required),
      'comment': new FormControl(null)
    });
  }

  onSubmit(){
    this._ordersService.createOrder(this.cartItems, this.orderForm.value);
    this.orderForm.reset();
  }

  ngOnDestroy() {
    this._subscription.unsubscribe();
  }

}
