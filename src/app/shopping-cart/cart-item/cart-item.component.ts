import { ShoppingCartService } from './../../shared/shopping-cart.service';
import { StockService } from './../../shared/stock.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CartItem } from './cart-item.model';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'cart-item',
  templateUrl: './cart-item.component.html',
  styleUrls: ['./cart-item.component.css']
})
export class CartItemComponent implements OnInit {
  @Input() public cartItem: CartItem;
  @Input() public index: number;
  public isValid: boolean = false;
  public quantityForm: FormGroup;
  private _quantityAvailable: number;

  constructor(private _stockService: StockService,
    private _shoppingCartService: ShoppingCartService) { }

  ngOnInit() {
    this._quantityAvailable = this._stockService.getProductQty(this.cartItem.product.id);
    this.quantityForm = new FormGroup({
      'quantity': new FormControl(this.cartItem.quantity,
        [Validators.required,
        Validators.pattern(/^[1-9]+[0-9]*$/),
        this.isQtyAvailable.bind(this)]),
    });

    this.isValid = this.quantityForm.valid;
  }

  private isQtyAvailable(control: FormControl): { [s: string]: boolean } | null {
    const value = +control.value;
    if (this._quantityAvailable - value < 0) {
      return { 'moreThanAvailable': true };
    }
    return null;
  }

  public onInput() {
    if (this.quantityForm.valid) {
      const newQtyReserved = this.quantityForm.get('quantity').value;
      this._stockService.setProductQtyReserved(this.cartItem.product.id, newQtyReserved);
      this.cartItem.updateQty(newQtyReserved);
      this.isValid = true;
      this._shoppingCartService.qtyInputChanged.emit();
    } else {
      this.isValid = false;
      this._shoppingCartService.qtyInputChanged.emit();
    }
  }

  onDelete() {
    this._shoppingCartService.deleteItemFromCart(this.index);
  }

}
