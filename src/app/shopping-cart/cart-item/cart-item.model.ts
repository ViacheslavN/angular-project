import { Product } from './../../shared/product.model';

export class CartItem {
    private _product: Product;
    private _quantity: number;
    private _price: number;
    private _total: number;

    constructor(product: Product, quantity: number, price: number) {
        this._product = product;
        this._quantity = quantity;
        this._price = price;
        this._total = this._price * this._quantity;
    }

    public get product(): Product {
        return this._product;
    }

    public get quantity(): number {
        return this._quantity;
    }

    public get price(): number {
        return this._price;
    }

    public get total(): number {
        return this._total;
    }

    public updateQty(newQty: number): void {
        if (newQty <= 0) {
            throw new Error('Invalid quantity!');
        }
        this._quantity = newQty;
        this._total = this._price * this._quantity;
    }
}