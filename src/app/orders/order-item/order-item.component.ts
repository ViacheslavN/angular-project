import { Order } from './order.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponent implements OnInit {
  @Input() public orderItem: Order;

  constructor() { }

  ngOnInit() {
  }

}
