import { CartItem } from './../../shopping-cart/cart-item/cart-item.model';
export class Order {
    private _cartItems: CartItem[];
    private _userInfo: { userFirstName: string, userLastName: string, email: string, phone: string, address: string, comment: string };
    private _total: number;
    private _status: string = 'pending';

    constructor(cartItems: CartItem[],
        userInfo: { userFirstName: string, userLastName: string, email: string, phone: string, address: string, comment: string }) {
        this._cartItems = cartItems;
        this._userInfo = userInfo;
        let total: number = 0;
        this._cartItems.forEach((cartItem: CartItem, index: number, array: CartItem[]) => {
            total += cartItem.total;
        });
        this._total = total;
    }

    public get cartItems(): CartItem[] {
        return this._cartItems;
    }

    public get userInfo(): { userFirstName: string, userLastName: string, email: string, phone: string, address: string, comment: string } {
        return this._userInfo;
    }

    public get total(): number {
        return this._total;
    }
}