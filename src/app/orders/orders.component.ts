import { Order } from './order-item/order.model';
import { OrdersService } from './../shared/orders.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  public orders: Order[];

  constructor(private _ordersService: OrdersService) { }

  ngOnInit() {
    this.orders = this._ordersService.getOrders();
  }

}
